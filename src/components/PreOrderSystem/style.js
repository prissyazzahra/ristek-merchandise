import styled from 'styled-components';

export const PreOrderSystemContainer = styled.div`
  padding: 4rem;
  vertical-align: middle;

  p {
    margin: 0;
    padding: 1rem;
    font-size: 24px;
    line-height: 42px;
  }

  img {
    margin: 2rem;
    width: 160px;
  }

  h2 {
    margin: 0;
    font-size: 36px;
  }

  .preorderbox {
    margin: 2rem 1rem 2rem 4rem;
    display: flex;
  }

  .pesan, .bayar, .proses {
    margin: 1rem;
    flex: 1;
  }

  @media screen and (max-width: 64em) {
    .preorderbox {
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    .pesan, .bayar, .proses {
      align-items: center;
      justify-content: center;
    }
  }

  @media screen and (max-width: 500px) {
    img {
      width: 100px;
    }

    h2 {
      font-size: 28px;
    }

    p {
      margin: 1rem 0;
      padding: 0;
      font-size: 20px;
    }
  }
`;