import React from 'react';
import PropTypes from 'prop-types';
import { PreOrderSystemContainer } from './style';
import Header from 'components/Header';
import pesanIcon from 'assets/images/pesan-icon.png';
import bayarIcon from 'assets/images/bayar-icon.png';
import prosesIcon from 'assets/images/proses-icon.png';

class PreOrderSystem extends React.Component {
  render() {
    return (
      <PreOrderSystemContainer>
        <section id="preOrder">
          <Header name="Pre-Order System" />
          <div className="preorderbox">
            <div className="pesan">
              <img alt="pesan" src={pesanIcon}></img>
              <h2>Pesan</h2>
              <p>User melakukan pemesanan produk melalui website</p>
            </div>
            <div className="bayar">
              <img alt="bayar" src={bayarIcon}></img>
              <h2>Bayar</h2>
              <p>Lakukan pembayaran sesuai nominal yang ditentukan</p>
            </div>
            <div className="proses">
              <img alt="proses" src={prosesIcon}></img>
              <h2>Proses</h2>
              <p>Pesanan akan diproses apabila jumlah yang mengorder telah mencapai jumlah minimal pre-order dan telah melewati batas waktu yang ditentukan</p>
            </div>
          </div>
        </section>
      </PreOrderSystemContainer>
    );
  }
}

PreOrderSystem.propTypes = {

};

export default PreOrderSystem;
