import styled from 'styled-components';

export const NavBarContainer = styled.div`
  padding: 1rem;
  width: 100%;
  position: absolute;
  z-index: 10000;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  a {
    color: ${props => props.theme.colors.white}
    text-decoration: none;
    cursor: pointer;
    margin: 0 3rem;
  }

  H1 {
    margin: 0;
    font-size: 1.6rem;
  }

  a:hover {
    color: ${props => props.theme.colors.yellow};
    transition: 0.2s ease-in;

    path {
      fill: ${props => props.theme.colors.yellow};
      transition: 0.2s ease-in;
    }

    .cart-total-number {
      background: ${props => props.theme.colors.white};
      transition: 0.2s ease-in;
    }
  }

  svg {
    width: 50px;
    height: auto;
  }

  .nav-wrapper {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .cart-total-number {
    display: inline-block;
    margin: -18px;
    padding: 8px;
    width: 36px;
    height: 36px;
    color: ${props => props.theme.colors.blue};
    background: ${props => props.theme.colors.yellow};
    border-radius: 50%;
    text-align: center;
    font-size: 24px;
  }

  .hamburger-menu {
    width: 2rem;
    height: 2rem;
    padding: 0.2rem;
    font-size: 1.5rem;
    background: white;
    text-align: center;
    color: ${props => props.theme.colors.yellow};
    border-radius: 4px;
    display: none;
    
  }

  .hamburger-menu:hover {
    color: white;
    background: ${props => props.theme.colors.yellow};
  }

  .hide {
    display: none;
  }

  @media screen and (max-width: 56em) {
    justify-content: space-between;

    .hamburger-menu {
      display: block;
    }

    .nav-wrapper {
      display: none;
    }

    .expand {
      width: 100vw;
      height: 100vh;
      position: fixed;
      z-index: 2;
      top: 0;
      left: 0;
      display: flex;
      flex-direction: column;
      background-color: ${props => props.theme.colors.blue};
    }

    .nav-wrapper a {
      text-align: left;
    }

    .nav-wrapper H1 {
      width: 100%;
      border-bottom: 3px solid ${props => props.theme.colors.yellow};
      padding: 1rem 0;
      margin: 0 2rem;
    }

    svg {
      width: 30px;
    }

    .cart-total-number {
      width: 22px;
      height: 22px;
      font-size: 15px;
      padding: 3px;
    }

    H1 {
      font-size: 1.2rem;
    }
  }
`;

export const CloseMenu = styled.div`
  display: none;

  @media screen and (max-width: 52em) {
    display: flex;
    align-items: center;
    text-align: center;
    padding: 0.5rem 3rem;
    margin: 1rem auto;
    border: 2px solid white;
    border-radius: 0.5rem;
    font-size: 1rem;
    cursor: pointer;
    color: ${props => props.theme.colors.white};
  }
`;