import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { NavBarContainer, CloseMenu } from './style';
import { H1, ShopBagSvg } from 'components/BasicStyledComponents'
import { getProductPhases } from '../../selectors/productSection';

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpanded: false,
    }
  }

  toggleExpand() {
    const { isExpanded } = this.state;
    this.setState({ isExpanded: !isExpanded });
  }

  render() {
    const { isExpanded } = this.state;
    let cart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];

    return (
      <NavBarContainer>
        <a
          className={isExpanded ? "hamburger-menu hide" : "hamburger-menu"}
          onClick={() => this.toggleExpand()}
        >
            <i className="fa fa-bars"></i>
        </a>
        <div className={isExpanded ? "nav-wrapper expand" : "nav-wrapper"}>
          <H1><a href="/">Home</a></H1>
          <H1><a href="#preOrder" onClick={() => this.toggleExpand()}>Pre-Order System</a></H1>
          <H1><a href="#products" onClick={() => this.toggleExpand()}>Products</a></H1>
          <CloseMenu onClick={() => this.toggleExpand()}>
            Close Menu
          </CloseMenu>
        </div>
          <H1><a onClick={() => this.props.push('/review')}>
          <ShopBagSvg />
          {
            cart.length !== 0 &&
            <div className="cart-total-number">
              {cart.length}
            </div>
          }
          </a></H1>
      </NavBarContainer>
    );
  }
}

NavBar.propTypes = {
  push: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
  };
}

export default connect(null, mapDispatchToProps)(NavBar);