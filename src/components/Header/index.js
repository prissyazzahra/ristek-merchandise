import React from 'react';
import PropTypes from 'prop-types';
import { H2 } from 'components/BasicStyledComponents';
import { HeaderContainer } from './style';

class Header extends React.Component {
  render() {
    return (
      <HeaderContainer>
        <H2>{ this.props.name }</H2>
        <div className="rectangle"></div>
      </HeaderContainer>
    );
  }
}

Header.propTypes = {
  "name": PropTypes.string.isRequired,
};

export default Header;
