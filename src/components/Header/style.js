import styled from 'styled-components';

export const HeaderContainer = styled.div`
  margin: 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  line-spacing: 10px;

  h2 {
    font-size: 42px;
    margin: 0;
  }

  .rectangle {
    margin: 1rem;
    width: 200px;
    height: 6px;
    background: ${props => props.theme.colors.yellow};
  }

  @media only screen and (max-width: 500px) {
    h2 {
      font-size: 28px;
    }
  }
`;