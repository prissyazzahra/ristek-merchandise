import React from 'react';
import PropTypes from 'prop-types';
import { H1 } from 'components/BasicStyledComponents';

import { LandingSectionContainer } from './style';

class LandingSection extends React.Component {

  render() {
    return (
      <LandingSectionContainer>
        <div className="container">
          <div className="box">
            <div className="title">
                <H1>RISTEK</H1>
                <H1>Merchandise</H1>
                <H1>2018</H1>
              </div>
          </div>
        </div>
          <div className="container-front">
            <div className="change-box">
              <div className="title">
                <H1>RISTEK</H1>
                <H1>Merchandise</H1>
                <H1>2018</H1>
              </div>
            </div>
          </div>
      </LandingSectionContainer>
    );
  }
}

LandingSection.propTypes = {

};

export default LandingSection;
