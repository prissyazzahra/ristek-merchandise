import styled from 'styled-components';
import BackgroundBlue from 'assets/images/landing-blue-lg.png';
import BackgroundYellow from 'assets/images/landing-yellow-lg2.png';
import BackgroundBlueResponsive from 'assets/images/landing-blue-lg-responsive.png'
import BackgroundYellowResponsive from 'assets/images/landing-yellow-lg2-responsive.png'


export const LandingSectionContainer = styled.div`
  position: relative;

  .box {
    height: 100vh;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    animation: slide2 15s infinite;
    background: url(${BackgroundBlue}) 0% 0% / cover;
  }

  .container, .container-front {
    width: 100%;
    top: 0;
    height: 100vh;
    overflow: hidden;
  }

  .container-front {
    position: absolute;
  }

  .change-box {
    transform: translateX(-100%);
    background: url(${BackgroundYellow}) 0% 0% / cover;
    height: 100vh;
    width: 100%;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    animation: slide 15s infinite;
  }

  @keyframes slide {
    0%, 30% {
      transform: translateX(-100%);
    }
    40%, 70% {
      transform: translateX(0);
    }
    80%, 100% {
      transform: translateX(100%);
    }
  }

  @keyframes slide2 {
    0%, 30%, 80%, 100% {
      transform: translateX(0);
    }
    40%, 60% {
      transform: translateX(100%);
    }
    70% {
      transform: translateX(-100%);
    }
  }

  .title {
    width: 40%;
    float: right;
  }

  H1 {
    margin:0;
    font-size: 4rem;
  }

  @media screen and (max-width: 64em) {
    .title {
      width: 20%;
      display: flex;
      justify-content: flex-end;
      transform: rotate(270deg);
    }

    .box {
      background: url(${BackgroundBlueResponsive}) 0% 0% / cover;
    }

    .change-box {
      background: url(${BackgroundYellowResponsive}) 0% 0% / cover;
    }

    H1 {
      font-size: 4vh;
      margin-right: 0.5rem;
    }
  }
`;