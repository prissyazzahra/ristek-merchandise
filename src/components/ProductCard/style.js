import styled from 'styled-components';

export const ProductCardContainer = styled.div`
  flex-basis: 50%;
  display: flex;
  align-items: stretch;

  .disabled {
    opacity: 0.5;
  }

  .card-container {
    cursor: pointer;
    margin: 2rem;
    padding: 1rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    border: 0.2px solid ${props => props.theme.colors.borderGray}

    p {
      font-size: 24px;
    }

    .product-image {
      margin: 1rem;
      width: 75%;
    }

    .product-detail {
      margin: auto;
    }

    .product-price {
      margin: 0;
      font-weight: bold;
    }

    .show-on-hover {
      display: none;
    }
  }

  .card-container:hover {
    width: 95%;
    height: auto%;
    box-shadow: ${props => props.theme.shadow.cardHover}

    .product-detail {
      display: none;
    }

    .show-on-hover {
      margin: auto;
      padding: 1rem;
      display: block;
      width: 100%;
      align-self: flex-start;

      .hover-label {
        margin: 0 0 0.5rem 0;
      }
    }
  }

  .blue {
    color: ${props => props.theme.colors.blue};
  }

  @media screen and (max-width: 550px) {
    flex-basis: 100%;

    .card-container {
      width: 100%;
      margin: 1rem;

      p {
        font-size: 20px;
      }

      .product-price {
        font-size: 18px;
      }
    }

    .product-image {
      width: 100%;
    }

  }

`;
