import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { ProductCardContainer } from './style'
import ProgressBar from 'components/ProgressBar'

class ProductCard extends React.Component {
  formatPrice(price) {
    let rupiah = '';
    let pricerev = price.toString().split('').reverse().join('');
    for (let i = 0; i < pricerev.length; i++) if (i % 3 == 0) rupiah += pricerev.substr(i, 3) + '.';
    return rupiah.split('', rupiah.length - 1).reverse().join('');
  }

  render() {

    const {
      id,
      name,
      photo_url,
      price,
      phase_set
    } = this.props.product;
    const phase_set_id = phase_set[phase_set.length - 1].id;

    let listOfBar = null;
    let isPhaseFull = false;
    if (phase_set) {
      listOfBar = phase_set.map(
        (phase, index) => {
          if (!phase.is_closed) {
            const { current_total_order, threshold_pre_order } = phase;
            isPhaseFull = current_total_order === threshold_pre_order;
            return <ProgressBar
              key={index}
              current_total_order={current_total_order}
              threshold_pre_order={threshold_pre_order}
            />
          } else {
            return <p className="blue">Thank you! phase {phase.phase_name} is closed</p>
          }
        }
      );
    }

    return (
      <ProductCardContainer>
        <div
          className={`${isPhaseFull ? 'card-container disabled' : "card-container" }`}
          onClick={`
            ${isPhaseFull ? ''
              : () => this.props.push({
                pathname: `/detail/${id}/${phase_set_id}`
              })
            }`
          }
        >
          <img className="product-image" src={photo_url} alt="Product"></img>
          <div className="product-detail">
            <p className="product-name">{name}</p>
            <p className="product-price blue">Rp {this.formatPrice(price)}</p>
          </div>
          <div className="show-on-hover">
            <p className="hover-label">Jumlah pre-order</p>
            {listOfBar}
          </div>
        </div>
      </ProductCardContainer>
    );
  }
}

ProductCard.defaultProps = {
  'productPrice': '-'
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    photo_url: PropTypes.string,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }).isRequired,
  push: PropTypes.func.isRequired
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
  };
}

export default connect(null, mapDispatchToProps)(ProductCard);