import React from 'react';
import PropTypes from 'prop-types';
import { ProgressBarContainer } from './style'

class ProgressBar extends React.Component {
  render() {
    const Filler = (props) =>
      <div
        className="filler"
        style={{
          background: `${this.props.color}`,
          width: `${props.percentage}%`
        }}
      />
    const { current_total_order, threshold_pre_order } = this.props;
    const percentage = (current_total_order / threshold_pre_order) * 100;

    return (
      <ProgressBarContainer>
        <div
          className="hover-bar"
          style={{
            border: `1px solid ${this.props.color}`
          }}
        >
          <Filler percentage={percentage} />
          <div className="blue right">{current_total_order}/{threshold_pre_order}</div>
        </div>
      </ProgressBarContainer>
    );
  }
}

ProgressBar.defaultProps = {
  color: '#4196D2'
}

ProgressBar.propTypes = {
  color: PropTypes.string,
  current_total_order: PropTypes.number,
  threshold_pre_order: PropTypes.number
}

export default ProgressBar;