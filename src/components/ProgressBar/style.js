import styled from 'styled-components';

export const ProgressBarContainer = styled.div`

  .hover-bar {
    margin: 1.6rem 0;
    background: ${props => props.theme.colors.white},
    position: relative;
    width: 100%;
    height: 20px;
  }

  .filler {
    height: 100%;
    transition: width .2s ease-in;
  }

  .right {
    font-size: 20px;
    text-align: right;
  }
`;