import React from 'react';
import styled from 'styled-components';

export const H1 = styled.h1`
    color: ${props => props.theme.colors.white}
    font-size: 2rem;
    font-family: 'Josefin Sans', sans-serif;
  `;

export const H2 = styled.h2`
    margin: 1rem;
    font-size: 2.8rem;
    letter-spacing: 6px;
    text-align: center;
  `;

export const ShopBagSvg = () =>
  <svg width="95" height="104" viewBox="0 0 95 104" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M94.525 21.7455C94.525 21.7455 94.525 21.2727 94.05 21.2727C94.05 21.2727 94.05 20.8 93.575 20.8L79.325 1.89091C78.85 0.472727 77.425 0 76 0H19C17.575 0 16.15 0.472727 15.2 1.89091L0.95 20.8C0.95 20.8 0.95 21.2727 0.475 21.2727C0.475 21.2727 0.475 21.7455 0 21.7455C0 22.6909 0 23.1636 0 23.6364V89.8182C0 97.8545 6.175 104 14.25 104H80.75C88.825 104 95 97.8545 95 89.8182V23.6364C95 23.1636 95 22.6909 94.525 21.7455ZM21.375 9.45455H73.625L80.75 18.9091H14.25L21.375 9.45455ZM80.75 94.5454H14.25C11.4 94.5454 9.5 92.6545 9.5 89.8182V28.3636H85.5V89.8182C85.5 92.6545 83.6 94.5454 80.75 94.5454Z" fill="#FFFFFF" />
    <path d="M66.5 37.8181C63.65 37.8181 61.75 39.709 61.75 42.5453C61.75 50.5817 55.575 56.7271 47.5 56.7271C39.425 56.7271 33.25 50.5817 33.25 42.5453C33.25 39.709 31.35 37.8181 28.5 37.8181C25.65 37.8181 23.75 39.709 23.75 42.5453C23.75 55.7817 34.2 66.1817 47.5 66.1817C60.8 66.1817 71.25 55.7817 71.25 42.5453C71.25 39.709 69.35 37.8181 66.5 37.8181Z" fill="#FFFFFF" />
  </svg>

export const CircleBG = () =>
  <svg width="1007" height="1080" viewBox="0 0 1007 1080" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle cx="336.5" cy="568.5" r="670.5" fill="#D7DEF2" fill-opacity="0.39" fill="rgba(215, 222, 242, 0.39)"/>
  </svg>
