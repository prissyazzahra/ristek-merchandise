import styled from 'styled-components';

export const OrderCardContainer = styled.div`
  width: 95%;
  margin-bottom: 2rem;
  padding: 1rem;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border: 0.2px solid ${props => props.theme.colors.borderGray}
  box-shadow: ${props => props.theme.shadow.cardHover}

  .column {
    flex-basis: 20%;
  }

  .product {
    display: flex;
    flex-direction: column;

    img {
      margin-left: 1rem;
      width: 50%;
    }

    span {
      margin: 1rem;
    }
  }

  .size {
    text-align: center;
  }

  .qty {
    text-align: center;

    a {
      cursor: pointer;
    }

    a:hover {
      background-color: ${props => props.theme.colors.lightGray};
    }

    .btn-min {
      padding: 10px;
      border-bottom-left-radius: 100px;
      border-top-left-radius: 100px;
      border: 0.7px solid ${props => props.theme.colors.borderGray};
    }

    .btn-plus {
      padding: 10px;
      border-bottom-right-radius: 100px;
      border-top-right-radius: 100px;
      border: 0.7px solid ${props => props.theme.colors.borderGray};
    }

    .btn-delete {
      color: ${props => props.theme.colors.red}
      padding: 10px;
      border-bottom-left-radius: 100px;
      border-top-left-radius: 100px;
      border: 0.7px solid ${props => props.theme.colors.borderGray};
    }

    input {
      width: 20%;
      padding: 9px;
      text-align: center;
      border: 0.7px solid ${props => props.theme.colors.borderGray};
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
  }

  .total {
    color: ${props => props.theme.colors.yellow};
    font-weight: bold;
    text-align: center;
  }
`;