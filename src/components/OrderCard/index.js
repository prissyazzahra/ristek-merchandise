import React from 'react';
import PropTypes from 'prop-types';

import { OrderCardContainer } from './style';

class OrderCard extends React.Component {
  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.qtyDecrease = this.qtyDecrease.bind(this);
    this.qtyIncrease = this.qtyIncrease.bind(this);
  }

  handleChange(event) {
    const target = event.target;

    this.updateValue(target.value);
  }

  handleDelete() {
    this.props.deleteMe(this.props.index);
  }

  qtyDecrease() {
    const value = this.props.productQty - 1;
    if (value !== 0) {
      this.updateValue(value);
    }
  }

  qtyIncrease() {
    const value = this.props.productQty + 1;

    this.updateValue(value);
  }

  updateValue(value) {
    //update local
    this.props.updateValue(value, this.props.index);
  }

  formatPrice(price) {
    let rupiah = '';
    let pricerev = price.toString().split('').reverse().join('');
    for (let i = 0; i < pricerev.length; i++) if (i % 3 == 0) rupiah += pricerev.substr(i, 3) + '.';
    return rupiah.split('', rupiah.length - 1).reverse().join('');
  }

  render() {

    return (
      <OrderCardContainer>
        <div className="product column">
          <img src={this.props.productImage} alt="product" />
          <span>{this.props.productName}</span>
        </div>

        <div className="size column">
          <span>{this.props.productSize}</span>
        </div>

        <div className="qty column">
          <div className="qty-input-container">
            {this.props.productQty != 1
              ? <a className="btn-min" onClick={this.qtyDecrease}>-</a>
              : <a className="btn-delete" onClick={this.handleDelete}>x</a>
            }
            <input type="number" value={this.props.productQty} name="productQty" onChange={this.handleChange} />
            <a className="btn-plus" onClick={this.qtyIncrease}>+</a>
          </div>
        </div>

        <div className="total column">
          <span>Rp. {this.formatPrice(this.props.productPrice)}</span>
        </div>
      </OrderCardContainer>
    );
  }
}

OrderCard.defaultProps = {
  productPrice: '-'
}

OrderCard.propTypes = {
  deleteMe: PropTypes.func.isRequired,
  productImage: PropTypes.string,
  productSize: PropTypes.string.isRequired,
  productName: PropTypes.string.isRequired,
  productPrice: PropTypes.number.isRequired,
  updateValue: PropTypes.func.isRequired
};

export default OrderCard;
