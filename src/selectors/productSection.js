export const isFetchPhasesLoading = (state) => state.productSectionReducer.get('isLoading');

export const isFetchPhasesLoaded = (state) => state.productSectionReducer.get('isLoaded');

export const getProductPhases = (state) => state.productSectionReducer.get('phases');