export const isSubmitOrdersSuccess = (state) => state.checkoutPageReducer.get('isSubmitSuccess');

export const getCheckoutPagePayload = (state) => state.checkoutPageReducer.get('payload');