export const getProductJSON = (state) => state.detailProductPageReducer.get('productJSON');

export const isDetailProductPageLoading = (state) => state.detailProductPageReducer.get('isLoading');

export const isDetailProductPageLoaded = (state) => state.detailProductPageReducer.get('isLoaded');