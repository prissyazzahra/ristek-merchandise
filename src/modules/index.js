/* eslint-disable */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import productSectionReducer from 'containers/ProductSection/reducer';
import detailProductPageReducer from 'containers/DetailProductPage/reducer';
import checkoutPageReducer from 'containers/CheckoutPage/reducer';

export default combineReducers({
  'detailProductPageReducer': detailProductPageReducer,
  'routing': routerReducer,
  'productSectionReducer': productSectionReducer,
  'checkoutPageReducer': checkoutPageReducer,
});
