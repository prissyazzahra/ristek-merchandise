export const SUBMIT_ORDER = 'src/CheckoutPage/SUBMIT_ORDER';

export const SUBMIT_ORDER_SUCCESS = 'src/CheckoutPage/SUBMIT_ORDER_SUCCESS';

export const SUBMIT_ORDER_FAILED = 'src/CheckoutPage/SUBMIT_ORDER_FAILED';