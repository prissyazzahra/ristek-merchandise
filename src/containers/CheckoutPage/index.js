import React from 'react';
import swal from 'sweetalert';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { CheckoutPageContainer } from './style';
import { H2 } from 'components/BasicStyledComponents';
import Ellipses from 'assets/images/Ellipses.png';
import { ShopBagSvg } from 'components/BasicStyledComponents'
import { submitOrders } from './actions';
import {
  getCheckoutPagePayload,
  isSubmitOrdersSuccess
} from 'selectors/checkoutPage'

class CheckoutPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cart: [],
      buyer_name: '',
      buyer_major: '',
      buyer_line_id: '',
      buyer_phone: '',
      warnings: {
        buyer_name: '',
        buyer_major: '',
        buyer_line_id: '',
        buyer_phone: '',
      }
    }

    this.handleChange = this.handleChange.bind(this);
    this.formValidation = this.formValidation.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    let cart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    if (cart.length !== 0) {
      this.setState({ cart: cart });
    }
  }

  componentDidUpdate() {
    // console.log(this.props.isSubmitSuccess);

    if (this.props.isSubmitSuccess) {
      const response = this.props.payload;
      console.log('res', response);
      if(response.status === 'success') {
        //clear local
        swal('Order Submitted!', "Your Cart will be cleared", "success");
        localStorage.clear();
      } else {
        swal('Failed to Submit Order :(', "please contact our contact person if problem persist", "error");
      }

      this.props.push('/thanks')
    }
  }

  handleChange(event) {
    const target = event.target;
    this.setState({ [target.name]: target.value });
  }

  formValidation() {
    const warnings = {
      buyer_name: '',
      buyer_major: '',
      buyer_line_id: '',
      buyer_phone: '',
    }

    let isValid = true;

    if(!this.state.buyer_name) {
      isValid = false;
      warnings.buyer_name = 'Please fill your name first';
    } else if(this.state.buyer_name.length > 256) {
      isValid = false;
      warnings.buyer_name = 'Name cannot be more than 256 characters';
    }

    if (this.state.buyer_major.length > 256) {
      isValid = false;
      warnings.buyer_major = 'Major cannot be more than 256 characters';
    }

    if(this.state.buyer_line_id.length > 128) {
      isValid = false;
      warnings.buyer_line_id = 'line ID cannot be more than 128 characters';
    }

    if (this.state.buyer_phone && (this.state.buyer_phone.length < 6 || isNaN(this.state.buyer_phone))) {
      isValid = false;
      warnings.buyer_phone = 'Phone number cannot be less than 6 digits and must be a number';
    } else if (this.state.buyer_phone && this.state.buyer_phone.length > 20) {
      isValid = false;
      warnings.buyer_phone = 'Phone number cannot be more than 20 digits';
    } else if (!this.state.buyer_phone) {
      isValid = false;
      warnings.buyer_phone = 'Please fill your Phone Number first';
    }

    this.setState({ warnings });

    return isValid;
  }

  handleSubmit() {
    if (this.formValidation()) {
      const cart = this.state.cart;
      let productList = [];
      cart.map(
        (item) => {
          const product = {
            product_size: item.product_size,
            order_amount: item.order_amount,
            phase_id: item.phase_id,
          }

          productList.push(product);
        }

      );
      let payload = {
        buyer_name: this.state.buyer_name,
        buyer_major: this.state.buyer_major,
        buyer_line_id: this.state.buyer_line_id,
        buyer_phone: this.state.buyer_phone,
        products: productList
      };
      console.log('toPost', payload)

      //post payload
      this.props.submitOrders(payload);
    }
  }

  render() {

    return (
      <CheckoutPageContainer>
        <img src={Ellipses} alt="Ellipses"></img>
        <div className="form-container">
          <H2>Checkout</H2>
          {
            this.state.cart.length !== 0
              ? <React.Fragment>
                <form>
                  <input type="text" value={this.state.buyer_name} name="buyer_name" placeholder="Name" onChange={this.handleChange} />
                  <div className="warning">{this.state.warnings.buyer_name}</div>

                  <input type="text" value={this.state.buyer_major} name="buyer_major" placeholder="Major" onChange={this.handleChange} />
                  <div className="warning">{this.state.warnings.buyer_major}</div>

                  <input type="text" value={this.state.buyer_line_id} name="buyer_line_id" placeholder="ID Line" onChange={this.handleChange} />
                  <div className="warning">{this.state.warnings.buyer_line_id}</div>

                  <input type="text" value={this.state.buyer_phone} name="buyer_phone" placeholder="Phone number" onChange={this.handleChange} />
                  <div className="warning">{this.state.warnings.buyer_phone}</div>

                </form>
                <div onClick={this.handleSubmit} className="submit-button">SUBMIT</div>
              </React.Fragment>

              : <React.Fragment>
                <div className="cant-checkout-detail">
                  <p>Opps, You Can't Checkout with an Empty Cart!</p>
                  <p>Come Check our Store <a href="/#products">here</a></p>
                </div>
                <div className="cart">
                  <ShopBagSvg />
                  <div className="cart-total-number">
                    :(
                  </div>
                </div>
              </React.Fragment>
          }

        </div>
        <div className="exit-button">
          <a onClick={this.props.history.goBack}>x</a>
        </div>

      </CheckoutPageContainer>
    );
  }
}

CheckoutPage.propTypes = {
  buyer_name: PropTypes.string,
  buyer_major: PropTypes.string,
  buyer_line_id: PropTypes.string,
  buyer_phone: PropTypes.string,
  history: PropTypes.object,
  push: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    isSubmitSuccess: isSubmitOrdersSuccess(state),
    payload: getCheckoutPagePayload(state),
  }
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
    submitOrders: (payload) => dispatch(submitOrders(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPage);
