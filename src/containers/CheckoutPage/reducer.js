import { fromJS } from 'immutable';
import {
  SUBMIT_ORDER,
  SUBMIT_ORDER_FAILED,
  SUBMIT_ORDER_SUCCESS
} from './constants';

const initialState = fromJS({
  error: null,
  isSubmitSuccess: false,
  payload: null,
});

function checkoutPageReducer(state = initialState, action) {
  switch (action.type) {
    case SUBMIT_ORDER:
      return state.set('');
    case SUBMIT_ORDER_SUCCESS:
      return state.set('isSubmitSuccess', true).set('payload', action.payload);
    case SUBMIT_ORDER_FAILED:
      return state.set('error', action.payload).set('isSubmitSuccess', false);
    default:
      return state;
  }
}

export default checkoutPageReducer;