import styled from 'styled-components';

export const CheckoutPageContainer = styled.div`
  width: 100%;
  position: relative;

  .warning {
    color: ${props => props.theme.colors.red};
  }

  img {
    height: 100vh;
    position: absolute;
    object-fit: cover;
    top: 0;
    left: 0;
    z-index: -1;
  }

  form {
    width: 100%;
    padding: 2rem;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }

  input {
    background-color: transparent;
    font-size: 1.2rem;
    width: 70%;
    padding: 10px 0;
    margin: 1rem 0;
    letter-spacing: 5px;
    border-style: none none solid none;
  }

  .submit-button {
    padding: 1rem;
    text-align: center;
    font-weight: bold;
    width: 70%;
    cursor: pointer;
    background: ${props => props.theme.colors.yellow};
    color: ${props => props.theme.colors.white};
    border-radius: 100px;
    border: 3px solid ${props => props.theme.colors.yellow};
  }

  .submit-button:hover {
    background: ${props => props.theme.colors.white};
    color: ${props => props.theme.colors.yellow};
    transition: 0.4s;
  }

  .form-container {
    width: 50%;
    height: 100vh;
    float: right;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    align-content: space-between;
  }

  .exit-button {
    position: absolute;
    right: 0;
    margin: 0 1rem;
    cursor: pointer;
    font-size: 72px;
    font-weight: normal;
  }

  .cart {
    top: 460px;
    width: 120px;
    height: 120px;
    padding: 8px 0 0 30px;
    background: ${props => props.theme.colors.blue}
    position: absolute;
    border-radius: 50%

    svg {
      position: relative;
      width: 60px;
    }

    path {
      fill: ${props => props.theme.colors.white};
    }

    .cart-total-number {
      position: absolute;
      top: 82px;
      left: 85px;
      display: inline-block;
      margin: -18px;
      padding: 8px;
      width: 36px;
      height: 36px;
      color: ${props => props.theme.colors.white};
      background: ${props => props.theme.colors.yellow};
      border-radius: 50%;
      text-align: center;
      font-size: 24px;
    }
  }

  .cant-checkout-detail {
    text-align: center;
    padding: 1rem;
  }

  @media screen and (max-width: 64em) {
    img {
      display: none;
    }

    .form-container {
      width: 100%;
    }
  }
`;