import axios from 'axios';
import {
  SUBMIT_ORDER,
  SUBMIT_ORDER_FAILED,
  SUBMIT_ORDER_SUCCESS
} from './constants';
import { submitOrdersApi } from 'api';

export function submitOrders(payload) {
  return (dispatch) => {
    dispatch({ type: SUBMIT_ORDER });

    return axios.post(submitOrdersApi, payload)
    .then((response) => {
      dispatch({
        payload: response.data,
        type: SUBMIT_ORDER_SUCCESS,
      });
    })
    .catch((error) => {
      dispatch({
        payload: error.response,
        type: SUBMIT_ORDER_FAILED,
      });
    });
  }
}