import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { ReviewOrderPageContainer } from './style';

import OrderCard from 'components/OrderCard';

class ReviewOrderPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cart: [],
    }

    this.deleteChild = this.deleteChild.bind(this);
    this.updateValue = this.updateValue.bind(this);
  }

  componentDidMount() {
    let cart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    if (cart.length !== 0) {
      this.setState({ cart: cart });
    }
  }

  deleteChild(index) {
    console.log('callback', index);
    let newCart = this.state.cart;
    newCart.splice(index, 1);
    localStorage.setItem('cart', JSON.stringify(newCart));
    this.setState({ cart: newCart });
  }

  updateValue(value, index) {
    //update local
    let cart = JSON.parse(localStorage.getItem('cart'));
    let updatedProduct = cart[index];
    updatedProduct["order_amount"] = value;
    cart.splice(index, 1, updatedProduct);
    localStorage.setItem('cart', JSON.stringify(cart));
    this.setState({ cart: cart });
  }

  formatPrice(price) {
    let rupiah = '';
    let pricerev = price.toString().split('').reverse().join('');
    for (let i = 0; i < pricerev.length; i++) if (i % 3 == 0) rupiah += pricerev.substr(i, 3) + '.';
    return rupiah.split('', rupiah.length - 1).reverse().join('');
  }

  render() {
    console.log('cart', this.state.cart);
    let totalPrice = 0;
    let listOfProducts = [];

    if (this.state.cart.length !== 0) {
      const cart = this.state.cart;
      cart.map(
        (cartItem, index) => {
          const productName = cartItem.product.name;
          const productSize = cartItem.product_size;
          const productImage = cartItem.product.photo_url;
          const productQty = cartItem.order_amount;
          const productPrice = cartItem.product.price * productQty;

          totalPrice += productPrice;

          return listOfProducts.push(
            <OrderCard
              key={index}
              index={index}
              productImage={productImage}
              productName={productName}
              productSize={productSize}
              productPrice={productPrice}
              productQty={productQty}
              deleteMe={this.deleteChild}
              updateValue={this.updateValue}
            />
          );
        }
      );
    } else {
      console.log('ayam');
      listOfProducts.push(
        <h2 className="empty-cart-info">Your Cart is Empty!</h2>
      );
    }
    console.log('list', listOfProducts);

    return (
      <ReviewOrderPageContainer>
        <div className="heading-container">
          <div className="exit-button">
            <a onClick={this.props.history.goBack}>x</a>
          </div>

          <div className="title">
            <h1>Review Order</h1>
          </div>
        </div>

        {
          this.state.cart.length !== 0
            ? <React.Fragment>
              <div className="list-heading">
                <h2>Produk</h2>
                <h2>Ukuran</h2>
                <h2>Kuantitas</h2>
                <h2>Total</h2>
              </div>

              <div className="card-list-container">
                {listOfProducts}
              </div>
            </React.Fragment>

            : listOfProducts
        }

        <div className="subtotal-container">
          <div className="subtotal">
            <h2>
              Subtotal:
              <span className="subtotal-price">Rp. {this.formatPrice(totalPrice)}</span>
            </h2>
          </div>
          <div className="checkout-button">
            {this.state.cart.length !== 0
              ? <a onClick={() => this.props.push('/checkout')}>CHECKOUT</a>
              : <span className="disabled">CHECKOUT</span>
            }
          </div>

        </div>
      </ReviewOrderPageContainer>
    );
  }
}

ReviewOrderPage.propTypes = {
  history: PropTypes.object,
  push: PropTypes.func,
};


function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
  };
}

export default connect(null, mapDispatchToProps)(ReviewOrderPage);
