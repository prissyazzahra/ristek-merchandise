import styled from 'styled-components';

export const ReviewOrderPageContainer = styled.div`
  padding: 2rem 4rem;

  .empty-cart-info {
    margin-top: 3rem;
    padding: 2rem;
    border: 2px solid ${props => props.theme.colors.borderGray};
    color: ${props => props.theme.colors.gray};
    text-align: center;
  }

  .heading-container {
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    .exit-button {
      cursor:pointer;
      align-self: flex-end;
      font-size: 72px;

      a {
        text-decoration: none;
        color: inherit;
      }
    }

    .title {
      font-size: 24px;

      h1 {
        margin: 0;
      }
    }
  }

  .list-heading {
    width: 95%;
    display: flex;
    padding: 2rem 2rem 1rem 2rem;
    justify-content: space-between;
  }

  .subtotal-container {
    display: flex;
    width: 95%;
    margin: 4rem;
    flex-direction: row;
    justify-content: flex-end;

    .subtotal {
      h2 {
        font-weight: normal;

        .subtotal-price {
          margin: 0 0.5rem;
          color: ${props => props.theme.colors.blue};
          font-weight: bold;
        }
      }
    }

    .checkout-button {
      margin-left: 1rem;
      align-self: center;

      a, span {
        padding: 0.5rem 3rem;
        cursor: pointer;
        color: ${props => props.theme.colors.blue};
        font-weight: bold;
        border: 2px solid ${props => props.theme.colors.blue};
        border-radius: 25px;
      }

      a:hover {
        background: ${props => props.theme.colors.blue};
        color: ${props => props.theme.colors.white};
        transition: 0.2s ease-in;
      }

      .disabled {
        cursor: initial;
        color: ${props => props.theme.colors.gray};
        border: 2px solid ${props => props.theme.colors.gray};
      }
    }
  }
`;