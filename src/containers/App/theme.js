export const theme = {
  'colors': {
    'black': '#333333',
    'blue': '#468DBC',
    'borderGray': '#CFCFD0',
    'brown': '#B4873B',
    'gray': 'rgba(51, 51, 49, 0.33)',
    'lightGray': 'rgba(0, 0, 0, 0.1)',
    'navy': '#294E60',
    'red': '#CC4040',
    'teal': '#4196D2',
    'white': '#FFFFFF',
    'yellow': '#F6BF3D'
  },
  'shadow': {
   'cardHover': '0px 2px 10px 5px rgba(215, 222, 242, 0.5);'
  }
};