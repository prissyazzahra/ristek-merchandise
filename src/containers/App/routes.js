import NotFoundPage from 'containers/NotFoundPage';
import HomePage from 'containers/HomePage'
import ReviewOrderPage from 'containers/ReviewOrderPage';
import DetailProductPage from 'containers/DetailProductPage';
import ProductSection from 'containers/ProductSection';
import CheckoutPage from 'containers/CheckoutPage';
import ThankYouPage from 'containers/ThankYouPage';

export const routes = [
{
  'component': HomePage,
  'exact': true,
  'path': '/'
},
{
  'component': DetailProductPage,
  'exact': true,
  'path': '/detail/:id/:phase_set_id'
},
{
  'component': ReviewOrderPage,
  'exact': true,
  'path': '/review'
},
{
  'component': ProductSection,
  'exact': true,
  'path': '/#Products'
},
{
  'component': CheckoutPage,
  'exact': true,
  'path': '/checkout'
},
{
  'component': ThankYouPage,
  'exact': true,
  'path': '/thanks'
},
  { component: NotFoundPage }
];
