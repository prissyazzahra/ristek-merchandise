import { fromJS } from 'immutable';
import {
  FETCH_PHASES,
  FETCH_PHASES_SUCCESS,
  FETCH_PHASES_FAILED
} from './constants';

const initialState = fromJS({
  error: null,
  isLoading: false,
  isLoaded: false,
  phases: {
    count: 0,
    next: null,
    previous: null,
    result: []
  },
});

function productSectionReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PHASES:
      return state.set('isLoading', true);
    case FETCH_PHASES_SUCCESS:
      return state.set('phases', action.payload).set('isLoading', false).set('isLoaded', true);
    case FETCH_PHASES_FAILED:
      return state.set('error', action.payload).set('isLoading', false);
    default:
      return initialState;
  }
}

export default productSectionReducer;