export const FETCH_PHASES = 'src/ProductSection/FETCH_PHASES';

export const FETCH_PHASES_SUCCESS = 'src/ProductSection/FETCH_PHASES_SUCCESS';

export const FETCH_PHASES_FAILED = 'src/ProductSection/FETCH_PHASES_FAILED';