import axios from 'axios';

import {
  FETCH_PHASES,
  FETCH_PHASES_FAILED,
  FETCH_PHASES_SUCCESS
} from './constants';
import { fetchPhasesApi } from 'api';

export function fetchPhases() {
  return (dispatch) => {
    dispatch({ type: FETCH_PHASES });
    axios.get(fetchPhasesApi)
      .then((response) => {
        dispatch({
          payload: response.data,
          type: FETCH_PHASES_SUCCESS,
        })
      })
      .catch((error) => {
        dispatch({
          payload: error.data,
          type: FETCH_PHASES_FAILED,
        })
      });
  };
}