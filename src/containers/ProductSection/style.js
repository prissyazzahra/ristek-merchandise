import styled from 'styled-components';

export const ProductSectionContainer = styled.div`
  padding: 1rem 5rem;

  .product-list {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    flex-wrap: wrap;
    align-items: stretch;
  }

  @media screen and (max-width: 550px) {
    .product-list {
      align-items: center;
      justify-content: center;
    }
  }
`;