import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ProductSectionContainer } from './style';
import Header from 'components/Header';
import ProductCard from '../../components/ProductCard';
import { fetchPhases } from './actions';
import {
  isFetchPhasesLoading,
  isFetchPhasesLoaded,
  getProductPhases
} from 'selectors/productSection';

class ProductSection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phasesResults: null
    }
  }

  componentDidMount() {
    if (!this.props.isLoaded) {
      this.props.fetchPhases();
      console.log(this.props.productPhases);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.productPhases !== prevProps.productPhases) {
      this.setState({ phasesResults: this.props.productPhases.results });
      console.log(this.state.phasesResults);
      console.log('prev', prevProps);
      console.log('curr', this.props);
    }
  }

  isEmptyProducts() {
    const { productPhases } = this.props;

    return productPhases.count === 0;
  }

  render() {
    let listOfProducts = null;
    const { isLoading } = this.props;
    const phasesResults = this.state.phasesResults
    if(this.state.phasesResults){
      console.log(this.state.phasesResults);
      listOfProducts = phasesResults.map(
        (product,index) =>
          <ProductCard
            key={index}
            product={product}
          />
        );
    }

    return (
      <ProductSectionContainer>
        <section id="products">
          <Header name="Products" />
          {
            this.isEmptyProducts()
            ? <div>
                <h1>No Products</h1>
              </div>
            : <React.Fragment>
              {
                isLoading ? <h2>loading...</h2>
                : <React.Fragment>
                    <div className="product-list">
                      {listOfProducts}
                    </div>
                  </React.Fragment>
              }
              </React.Fragment>
          }
        </section>
      </ProductSectionContainer>
    );
  }
}

ProductSection.propTypes = {
  isLoaded: PropTypes.bool,
  isLoading: PropTypes.bool,
  productPhases: PropTypes.object
};

function mapStatetoProps(state) {
  return {
    isLoaded: isFetchPhasesLoaded(state),
    isLoading: isFetchPhasesLoading(state),
    productPhases: getProductPhases(state),
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchPhases: () => dispatch(fetchPhases())
  };
}


export default connect(mapStatetoProps, mapDispatchToProps)(ProductSection);