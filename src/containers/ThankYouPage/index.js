import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import { ThankYouPageContainer } from './style';
import { H2 } from 'components/BasicStyledComponents';
import Ellipses from 'assets/images/Ellipses.png';

class ThankYouPage extends React.Component {
  render() {
    return (
      <ThankYouPageContainer>
        <img src={Ellipses} alt="Ellipses"></img>
        <div className="thank-you-container">
          <H2>Terima kasih!</H2>
          <p>Kami sudah menaruh anda dalam waiting list pre- order merchandise. Ikuti prosedur di bawah ini untuk melanjutkan.</p>
          <p>1. Lakukan pembayaran ke nomor <b>38746237462</b></p>
          <p>2. Konfirmasi pembayaran via line dengan menghubungi <b>CP: namanya</b></p>
        </div>
        <div className="exit-button">
          <a onClick={() => this.props.push('/')}>x</a>
        </div>
      </ThankYouPageContainer>
    );
  }
}

ThankYouPage.propTypes = {
  history: PropTypes.object,
  push: PropTypes.func.isRequired,
};


function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
  };
}

export default connect(null, mapDispatchToProps)(ThankYouPage);
