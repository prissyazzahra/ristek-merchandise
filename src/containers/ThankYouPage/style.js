import styled from 'styled-components';

export const ThankYouPageContainer = styled.div`
  width: 100%;
  position: relative;
  overflow: hidden;

  H2 {
    font-size: 56px;
  }

  p {
    font-size: 20px;
    line-height: 2rem;
  }

  img {
    height: 100vh;
    position: absolute;
    object-fit: cover;
    top: 0;
    left: 0;
    z-index: -1;
  }

  .thank-you-container {
    padding: 2rem 2rem 2rem 3rem;
    width: 50%;
    height: 100vh;
    float: right;
    display: flex;
    justify-content: center;
    flex-direction: column;
  }

  .exit-button {
      position: absolute;
      right: 0;
      margin: 0 1rem;
      cursor: pointer;
      font-size: 72px;
      font-weight: normal;
    }

    @media screen and (max-width: 64em) {
    img {
      display: none;
    }

    .thank-you-container {
      width: 100%;
      overflow-wrap: break-word;
    }
  }

`;