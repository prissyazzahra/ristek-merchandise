import { fromJS } from 'immutable';
import {
  FETCH_PRODUCT_DETAIL,
  FETCH_PRODUCT_DETAIL_SUCCESS,
  FETCH_PRODUCT_DETAIL_FAILED
} from './constants';

const initialState = fromJS({
  error: null,
  isLoading: false,
  isLoaded: false,
  productJSON: {
    color_hex_code: '',
    color_name: '',
    description: '',
    name: '',
    photo_url: '',
    price: 0,
    sizes: null
  }
})

function detailProductPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PRODUCT_DETAIL:
      return state.set('isLoading', true);
    case FETCH_PRODUCT_DETAIL_SUCCESS:
      return state.set('productJSON', action.payload).set('isLoading', false).set('isLoaded', true);
    case FETCH_PRODUCT_DETAIL_FAILED:
      return state.set('error', action.payload).set('isLoading', false).set('isLoaded', false);
    default:
      return initialState;
  }
}

export default detailProductPageReducer;