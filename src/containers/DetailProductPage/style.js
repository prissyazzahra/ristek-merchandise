import styled from 'styled-components';

export const DetailProductPageContainer = styled.div`
  width: 100%;
  height: 100%:
  overflow: hidden;
  display: flex;
  flex-direction: row;

  .cart {
    left: 15px;
    position: absolute;
    text-decoration: none;
    cursor: pointer;

    svg {
      position: relative;
      width: 60px;
    }

    path {
      fill: ${props => props.theme.colors.blue};
    }

    .cart-total-number {
      position: absolute;
      top: 75px;
      left: 55px;
      display: inline-block;
      margin: -18px;
      padding: 8px;
      width: 36px;
      height: 36px;
      color: ${props => props.theme.colors.white};
      background: ${props => props.theme.colors.blue};
      border-radius: 50%;
      text-align: center;
      font-size: 24px;
    }
  }

  .cart:hover {
    color: ${props => props.theme.colors.yellow};
    transition: 0.2s ease-in;

    path {
      fill: ${props => props.theme.colors.blue};
      transition: 0.2s ease-in;
    }

    .cart-total-number {
      background: ${props => props.theme.colors.yellow};
      transition: 0.2s ease-in;
    }
  }

  .product-picture-container {
    margin-right: 2rem;
    height: 100vh;
    flex-basis: 50%;

    .product-picture-bg {

      .picture-bg {
        width: 45%;
        height: 100%;
        position: absolute;
        z-index: -10;
      }
      .product-picture {
        .picture {
          width: 30%;
          position: absolute;
          top:15%;
          left:5%;
        }
      }
    }
  }

  .product-detail-container {
    margin: 1rem;
    width: 45%;

    .label {
      color: #474747;
      font-weight: bold;
    }

    .product-detail {

      .product-name {
        color: ${props => props.theme.colors.yellow};
      }

      .price {
        color: #474747;
        font-weight: bold;
        font-size: 24px;
      }

      .size-selector-container {

        .size-selector {
          display: flex;
          flex-direction: row;

          #M {
            background: ${props => props.theme.colors.lightGray};
          }

          .option {
            padding: 1rem;
            border: 0.2px solid ${props => props.theme.colors.borderGray};
          }

          .option:hover {
            cursor: pointer;
            background: ${props => props.theme.colors.lightGray};
          }

          .option:active {
            background: ${props => props.theme.colors.lightGray};
          }
        }
      }
    }

    .row-container {
      display: flex;
      flex-direction: row;
      justify-content: space-between;

      .qty-container {

        .qty {
          a {
            cursor: pointer;
          }

          a:hover {
            background-color: ${props => props.theme.colors.lightGray};
          }

          .btn-min {
            padding: 10px;
            border-bottom-left-radius: 100px;
            border-top-left-radius: 100px;
            border: 0.7px solid ${props => props.theme.colors.borderGray};
          }

          .btn-plus {
            padding: 10px;
            border-bottom-right-radius: 100px;
            border-top-right-radius: 100px;
            border: 0.7px solid ${props => props.theme.colors.borderGray};
          }

          input {
            width: 20%;
            padding: 9px;
            text-align: center;
            border: 0.7px solid ${props => props.theme.colors.borderGray};
          }

          input::-webkit-outer-spin-button,
          input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
          }
        }
      }

      .color-container {

        .color-detail-container {
          display: flex;
          flex-direction: row;
          align-items: center;

          .color-circle {
            margin: 0.5rem;
            width: 25px;
            height: 25px;
            border-radius: 25px;
          }

          .color-name {

          }
        }
      }
    }

    .checkout-button {
      width: 100%;
      margin-top: 2rem;
      padding: 1rem 3rem;
      text-align: center;
      cursor: pointer;
      color: ${props => props.theme.colors.blue};
      font-weight: bold;
      border: 2px solid ${props => props.theme.colors.blue};
      border-radius: 25px;
    }

    .checkout-button:hover {
      background: ${props => props.theme.colors.blue};
      color: ${props => props.theme.colors.white};
      transition: 0.2s ease-in;
    }

  }

  .exit-button {
    margin: 0 1rem;
    cursor: pointer;
    font-size: 72px;
    font-weight: normal;

    a {
      text-decoration: none;
      color: inherit;
    }
  }

  @media only screen and (max-width: 600px) {
    max-width: 100vw;
    flex-direction: column;
    
    p {
      font-size: 12px;
    }

    .product-picture-container {
      padding: 1rem;
      margin: 2rem auto 0;
      width: 50%;
      height: 100%;
      background: rgba(215, 222, 242, 0.39);
      border-radius: 50%;
      text-align: center;

      .product-picture-bg {

        .picture-bg {
          display:none;
        }

        .product-picture {
          .picture {
            width: 100%;
            position: relative;
            left: 0;
            top: 0;
            
          }
        }
      }
    }

    .product-detail-container {
      width: 90%;
      padding: 0 1rem;

      .product-detail {

        .price {
          font-size: 18px;
        }
      }

    }

    .exit-button {
      position: absolute;
      right: 0;
      top: 0;
      margin: 2rem 2rem 0 0 ;
      font-size: 36px;
    }
  }
`;