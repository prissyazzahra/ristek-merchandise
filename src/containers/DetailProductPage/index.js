import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { DetailProductPageContainer } from './style';

import ProgressBar from 'components/ProgressBar';
// import productImage from 'assets/images/blue-shirt.png';
import halfCircleBG from 'assets/images/half-circle-bg.png';

import { fetchProductDetail } from './actions';
import {
  getProductJSON,
  isDetailProductPageLoading,
  isDetailProductPageLoaded
} from 'selectors/productDetailPage'
import { ShopBagSvg } from 'components/BasicStyledComponents'
import swal from 'sweetalert';

class DetailProductPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      productSize: 'M',
      productQty: 1,
      cartAmount: 0
    }

    this.handleChange = this.handleChange.bind(this);
    this.qtyDecrease = this.qtyDecrease.bind(this);
    this.qtyIncrease = this.qtyIncrease.bind(this);
    this.renderSizes = this.renderSizes.bind(this);
    this.handleBuy = this.handleBuy.bind(this);
    this.handleChangeSize = this.handleChangeSize.bind(this);
  }

  componentDidMount() {
    if (!this.props.isLoaded) {
      const product_id = this.props.match.params.id;
      // console.log('id', this.props.match);
      // console.log('product_id', product_id);
      // console.log('phase_set_id', phase_set_id);

      this.props.fetchProductDetail(product_id);
      // console.log('fetch')
      let cart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
      if (cart.length != 0) {
        this.setState({ cartAmount: cart.length })
      }
    }
  }

  handleChange(event) {
    const target = event.target;
    this.setState({ [target.name]: target.value })
  }

  qtyDecrease() {
    const value = this.state.productQty - 1;
    if (value !== 0) {
      this.setState({ productQty: value });
    }
  }

  qtyIncrease() {
    const value = this.state.productQty + 1;
    this.setState({ productQty: value });

    // console.log('localStoriage', localStorage.getItem('cart'));
  }

  handleChangeSize(size) {
    // console.log(document.getElementById(size).style);
    document.getElementById(this.state.productSize).style.background = "none";
    document.getElementById(size).style.backgroundColor = "rgba(0, 0, 0, 0.1)";
    this.setState({ productSize: size });
  }

  handleBuy() {
    let cart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];

    const payload = {
      product_size: this.state.productSize,
      order_amount: this.state.productQty,
      phase_id: this.props.match.params.phase_set_id,
      product: this.props.productJSON
    };

    cart.push(payload);
    console.log('to save', cart);
    localStorage.setItem('cart', JSON.stringify(cart));

    this.setState({ cartAmount: cart.length });
    swal("Item Added to cart!", "", "success");
  }

  renderSizes(sizes) {
    let listOfSizes = [];
    if (sizes) {
      for (let index = 0; index < sizes.length; index++) {
        listOfSizes.push(
          <div
            className="option"
            id={sizes[index]}
            onClick={() => this.handleChangeSize(sizes[index])}
          >
            {sizes[index]}
          </div>
        );
      }
    }

    return listOfSizes;
  }

  formatPrice(price) {
    let rupiah = '';
    let pricerev = price.toString().split('').reverse().join('');
    for (let i = 0; i < pricerev.length; i++) if (i % 3 == 0) rupiah += pricerev.substr(i, 3) + '.';
    return rupiah.split('', rupiah.length - 1).reverse().join('');
  }

  render() {
    // console.log(this.props.productJSON);
    const product = this.props.productJSON;
    const list = product.sizes;
    // console.log('ini',product.price)
    return (
      !this.props.productJSON ? <h2>Loading.. please wait</h2>
        : <DetailProductPageContainer>
          <a
            className="cart"
            onClick={() => this.props.push('/review')}
          >
            <ShopBagSvg />
            {
              this.state.cartAmount !== 0 &&
              <div className="cart-total-number">
                {this.state.cartAmount}
              </div>
            }
          </a>
          <div className="product-picture-container">
            <div className="product-picture-bg">
              <img className="picture-bg" src={halfCircleBG} />
              <div className="product-picture">
                <img className="picture" src={product.photo_url} alt="detail-picture" />
              </div>
            </div>
          </div>

          <div className="product-detail-container">
            <div className="product-detail">
              <h1 className="product-name">
                {product.name}
              </h1>

              <p className="description">
                {product.description}
              </p>

              <p className="price">
                Rp. {product.price ? this.formatPrice(product.price) : product.price}
              </p>

              <div className="size-selector-container">
                <p className="label">Pilih Ukuran:</p>
                <div className="size-selector">
                  {
                    this.renderSizes(list)
                  }
                </div>
              </div>

              <div className="row-container">
                <div className="qty-container">
                  <p className="label">Kuantitas:</p>

                  <div className="qty">
                    <div className="qty-input-container">
                      <a className="btn-min" onClick={this.qtyDecrease}>-</a>
                      <input type="number" value={this.state.productQty} name="productQty" onChange={this.handleChange} />
                      <a className="btn-plus" onClick={this.qtyIncrease}>+</a>
                    </div>
                  </div>
                </div>

                <div className="color-container">
                  <p className="label">Warna:</p>
                  <div className="color-detail-container">
                    <div className="color-circle" style={{ background: `${product.color_hex_code}` }}></div>
                    <span className="color-name">{product.color_name}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="pre-order-qty-container">
              <p className="label">Jumlah pre-order:</p>
              <ProgressBar color="#F6BF3D" />
            </div>
            <div
              className="checkout-button"
              onClick={this.handleBuy}
            >
              BELI SEKARANG
            </div>
          </div>

          <div className="exit-button">
            <a onClick={this.props.history.goBack}>x</a>
          </div>
        </DetailProductPageContainer>
    );
  }
}

DetailProductPage.defaultProps = {
  productDescription: 'ini default',
  productName: 'Default',
  productPrice: '-',
  productColor: 'Navy Blue',
  productColorHex: '#294E60',
  push: PropTypes.func.isRequired,
}

DetailProductPage.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object,
  productDescription: PropTypes.string,
  productJSON: PropTypes.object,
  productName: PropTypes.string,
  productPrice: PropTypes.string,
  productColor: PropTypes.string,
  productColorHex: PropTypes.string,
  push: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  isLoaded: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    productJSON: getProductJSON(state),
    isLoading: isDetailProductPageLoading(state),
    isLoaded: isDetailProductPageLoaded(state),
    push: PropTypes.func.isRequired
  }
}

function mapDispatchToProps(dispatch) {
  return {
    push: (url) => dispatch(push(url)),
    fetchProductDetail: (product_id) => dispatch(fetchProductDetail(product_id)),
    push: (url) => dispatch(push(url)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailProductPage);
