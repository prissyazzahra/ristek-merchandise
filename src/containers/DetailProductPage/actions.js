import axios from 'axios';

import {
  FETCH_PRODUCT_DETAIL,
  FETCH_PRODUCT_DETAIL_SUCCESS,
  FETCH_PRODUCT_DETAIL_FAILED
} from './constants';
import { fetchProductDetailApi } from 'api';

export function fetchProductDetail(product_id) {
  return (dispatch) => {
    dispatch({ type: FETCH_PRODUCT_DETAIL });
    axios.get(fetchProductDetailApi(product_id))
      .then((response) => {
        dispatch({
          payload: response.data,
          type: FETCH_PRODUCT_DETAIL_SUCCESS,
        })
      })
      .catch((error) => {
        dispatch({
          payload: error.data,
          type: FETCH_PRODUCT_DETAIL_FAILED,
        })
      });
  };
}