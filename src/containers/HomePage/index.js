import React from 'react';
import { connect } from 'react-redux';
import { HomePageContainer } from './style';
import PreOrderSystem from 'components/PreOrderSystem';
import ProductSection from '../ProductSection';
import NavBar from 'components/NavBar';
import LandingSection from 'components/LandingSection';

class HomePage extends React.Component {
  render() {

    return (
      <HomePageContainer>
        <NavBar />
        <LandingSection />
        <PreOrderSystem />
        <ProductSection />
      </HomePageContainer>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(HomePage);
