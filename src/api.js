let API_PREFIX_URL = "http://localhost:8000/ristek-merchandise-api";

// if(process.env.NODE_ENV === 'production') {
//     // insert production api
// }

export const fetchAllProductsApi = `${API_PREFIX_URL}/products/`;

export function fetchProductDetailApi(product_id) {
    return `${API_PREFIX_URL}/products/${product_id}/`;
}

export const fetchPhasesApi = `${API_PREFIX_URL}/products/phases/`;

export const submitOrdersApi = `${API_PREFIX_URL}/orders/submit/`;